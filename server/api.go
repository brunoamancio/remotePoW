package main

import (
	"./powEngine/dcurlpidiver"
	"github.com/gin-gonic/gin"
)

func doPoW(c *gin.Context, trytes string, mwm int) {
	trytesWithPow, powErr := powengine.DoPoW("", trytes, mwm)
	if powErr != nil {
		respondWithError(c, powErr)
	} else {
		c.JSON(200, gin.H{
			"trytesWithPow": trytesWithPow,
			"mwm":           mwm,
		})
	}
}

func getServerVersion(c *gin.Context) {
	c.JSON(200, gin.H{
		"serverVersion": serverVersion,
	})
}

func getPoWType(c *gin.Context) {
	c.JSON(200, gin.H{
		"powType": powType,
	})
}

func getPoWVersion(c *gin.Context) {
	c.JSON(200, gin.H{
		"powVersion": powVersion,
	})
}

func getPoWInfo(c *gin.Context) {
	c.JSON(200, gin.H{
		"serverVersion": serverVersion,
		"powType":       powType,
		"powVersion":    powVersion,
	})
}
