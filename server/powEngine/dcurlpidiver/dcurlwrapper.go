package powengine

// #cgo CFLAGS: -Wall -fPIC -Wno-unused-variable -Wno-overflow -Wno-misleading-indentation -Wno-pointer-sign
// #include <stdlib.h>
// #include "pow_fpga.h"
import "C"
import (
	"unsafe"

	"gitlab.com/brunoamancio/remotePoW/logs"
)

func DoPoW(powServerAddress string, trytes string, mwm int) (string, error) {
	nonce := executePowFunc(trytes, mwm)
	return nonce, nil
}

func InitializePowEngine() {
	C.pow_fpga_init()
	logs.Log.Debug("Initialized PoW engine")
}

func executePowFunc(trytes string, mwm int) string {
	cTrytes := C.CString(trytes)
	defer C.free(unsafe.Pointer(cTrytes))

	cMWM := C.int(mwm)

	logs.Log.Debug("Initialized PoW")
	cResult := C.GoPowFPGA(cTrytes, cMWM)
	defer C.free(unsafe.Pointer(cResult))
	logs.Log.Debug("Ended PoW")

	goResult := C.GoString(cResult)
	logs.Log.Debugf("Resulting Trytes: %s", goResult)

	return goResult
}

func Dispose() {
	C.pow_fpga_destroy()
	logs.Log.Debug("Destroyed PoW engine")
}
