package main

import (
	"context"
	"errors"
	"net/http"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/brunoamancio/remotePoW/common"
	"gitlab.com/brunoamancio/remotePoW/logs"
	powengine "gitlab.com/brunoamancio/remotePoW/server/powEngine/dcurlpidiver"
)

const (
	serverVersion = "0.8-alpha"
	powType       = "remote"
	powVersion    = "0.8-alpha"
)

var (
	deathWaitGroup = &sync.WaitGroup{}
)

func main() {
	deathWaitGroup.Add(1)

	logs.Init("INFO")
	go startDeathSignalWatcher()
	powengine.InitializePowEngine()

	gin.SetMode(gin.ReleaseMode)
	engine := gin.Default()

	engine.POST("/", func(c *gin.Context) {
		if disposing {
			respondWithError(c, errors.New("Server not available"))
		}

		var getCommandRequest *common.GetCommandRequest
		if err := c.ShouldBindJSON(&getCommandRequest); err == nil {
			executeCommand(c, getCommandRequest)
		} else {
			respondWithError(c, errors.New("Unknown command"))
		}
	})

	go serveHTTP(engine)
	deathWaitGroup.Wait()
}

var srv *http.Server

func serveHTTP(api *gin.Engine) {
	srv = &http.Server{
		Addr:    "0.0.0.0:16000",
		Handler: api,
	}

	if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
		logs.Log.Fatal("API Server Error", err)
	}
}

func executeCommand(c *gin.Context, commandRequest *common.GetCommandRequest) {

	switch commandRequest.Command {
	case "doPow":
		doPoW(c, commandRequest.Trytes, commandRequest.MWM)
		break
	case "getServerVersion":
		getServerVersion(c)
		break
	case "getPoWType":
		getPoWType(c)
		break
	case "getPoWVersion":
		getPoWVersion(c)
		break
	case "getPoWInfo":
		getPoWInfo(c)
		break
	default:
		respondWithError(c, errors.New("Unknown command"))
	}
}

func respondWithError(c *gin.Context, err error) {
	c.JSON(http.StatusBadRequest, gin.H{"error": err})
}

func startDeathSignalWatcher() {
	ch := make(chan os.Signal, 1)
	signal.Notify(ch, os.Interrupt, syscall.SIGTERM, syscall.SIGINT, os.Kill)

	<-ch // waits until receives something to unblock

	finalizeGracefulDeath()
}

func finalizeGracefulDeath() {
	dispose()

	deathWaitGroup.Add(-1)
}

var disposing = false

func dispose() {
	disposing = true

	disposeSrv()
	powengine.Dispose()
}

func disposeSrv() {
	if srv != nil {
		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		if err := srv.Shutdown(ctx); err != nil {
			logs.Log.Fatal("API Server Shutdown Error:", err)
		}
		logs.Log.Debug("API Server exited")
		cancel()
	}
}
