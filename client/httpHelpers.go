package remotepow

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"time"
)

func sendRequest(request interface{}, url string) (responseBytes []byte, err error) {
	req, err := generateRequest(request, url)
	if err != nil {
		return nil, err
	}

	client := getClient(req)
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	responseBodyBytes, _ := ioutil.ReadAll(resp.Body)
	return responseBodyBytes, nil
}

func getClient(request *http.Request) *http.Client {
	client := &http.Client{Timeout: time.Second * 15}
	return client
}

func generateRequest(request interface{}, url string) (*http.Request, error) {
	requestBytes, _ := json.Marshal(request)

	req, err := http.NewRequest("POST", url, bytes.NewBuffer(requestBytes))
	req.Header.Set("X-IOTA-POW-VERSION", "1")
	req.Header.Set("Content-Type", "application/json")

	return req, err
}
