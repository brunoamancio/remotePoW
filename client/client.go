package remotepow

import (
	"encoding/json"
	"errors"

	"gitlab.com/brunoamancio/remotePoW/common"
)

func DoRemotePoW(powServerAddress string, stringTrytes string, minWeightMagnitude int) (string, error) {
	if powServerAddress == "" || stringTrytes == "" {
		return "", errors.New("Invalid pow server address or empty input trytes")
	}

	doPowRequest := &common.GetCommandRequest{Command: "doPow", Trytes: stringTrytes, MWM: minWeightMagnitude}

	doPowResponseBytes, err := sendRequest(doPowRequest, powServerAddress)

	var doPowResponse *common.DoPowResponse
	json.Unmarshal(doPowResponseBytes, &doPowResponse)

	return doPowResponse.TrytesWithPow, err
}

func GetServerVersion(powServerAddress string) (serverVersion string, Error error) {
	getServerVersionRequest := &common.GetCommandRequest{Command: "getServerVersion"}

	getServerVersionResponseBytes, err := sendRequest(getServerVersionRequest, powServerAddress)

	var getServerVersionResponse *common.GetServerVersionResponse
	json.Unmarshal(getServerVersionResponseBytes, &getServerVersionResponse)

	return getServerVersionResponse.ServerVersion, err
}

func GetPoWType(powServerAddress string) (powType string, Error error) {
	getPoWTypeRequest := &common.GetCommandRequest{Command: "getPoWType"}

	getPoWTypeResponseBytes, err := sendRequest(getPoWTypeRequest, powServerAddress)

	var getPoWTypeResponse *common.GetPoWTypeResponse
	json.Unmarshal(getPoWTypeResponseBytes, &getPoWTypeResponse)

	return getPoWTypeResponse.PoWType, err
}

func GetPoWVersion(powServerAddress string) (powVersion string, Error error) {
	getPoWVersionRequest := &common.GetCommandRequest{Command: "getPoWVersion"}

	getPoWVersionResponseBytes, err := sendRequest(getPoWVersionRequest, powServerAddress)

	var getPoWVersionResponse *common.GetPoWVersionResponse
	json.Unmarshal(getPoWVersionResponseBytes, &getPoWVersionResponse)

	return getPoWVersionResponse.PoWVersion, err
}

func GetPoWInfo(powServerAddress string) (serverVersion string, powType string, powVersion string, Error error) {
	getPoWInfoRequest := &common.GetCommandRequest{Command: "getPoWInfo"}

	getPoWInfoResponseBytes, err := sendRequest(getPoWInfoRequest, powServerAddress)

	var getPoWInfoResponse *common.GetPoWInfoResponse
	json.Unmarshal(getPoWInfoResponseBytes, &getPoWInfoResponse)

	return getPoWInfoResponse.ServerVersion, getPoWInfoResponse.PoWType, getPoWInfoResponse.PoWVersion, err
}
