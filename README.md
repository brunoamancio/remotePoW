# Remote PoW
Allows you to delegate PoW to remote servers

## Install go

ARM:
```
wget https://dl.google.com/go/go1.11.linux-armv6l.tar.gz && tar -xvf  go1.11.linux-armv6l.tar.gz && sudo mv go /usr/local && rm go1.11.linux-armv6l.tar.gz
```
Amd64:
```
wget https://dl.google.com/go/go1.11.linux-amd64.tar.gz && tar -xvf go1.11.linux-amd64.tar.gz && sudo mv go /usr/local && rm go1.11.linux-amd64.tar.gz
```
Other versions: https://golang.org/dl/

## Add go to PATHs

```
nano ~/.profile
```

Save content in it:
```
export GOROOT=/usr/local/go
export GOPATH=$HOME/go
export PATH=$GOPATH/bin:$GOROOT/bin:$PATH
```
Update the file
```
source ~/.profile
```

## Clone and build Remote PoW's repository

```
git clone https://gitlab.com/brunoamancio/remotePoW.git && cd remotePoW
go get -d ./... && mkdir build && go build -o build/remotePowServer ./server
```

## Run Remote PoW as a service

```
sudo nano /lib/systemd/system/remotepowserver.service
```

Save content in it:
```
[Unit]
Description=IOTA remote pow server
After=network.target

[Service]
WorkingDirectory=/home/USERNAMEHERE/remotePoW/build
User=USERNAMEHERE
PrivateDevices=no
ProtectSystem=full
Type=simple
ExecReload=/bin/kill -HUP
KillMode=mixed
KillSignal=SIGTERM
TimeoutStopSec=60
ExecStart=/home/USERNAMEHERE/remotePoW/build/remotePowServer
SyslogIdentifier=remotepowserver
Restart=on-failure
RestartSec=30

[Install]
WantedBy=multi-user.target
Alias=remotepowserver.service

```
Reload services
```
sudo systemctl daemon-reload && sudo systemctl enable remotepowserver.service
```

## Add aliases
```
nano ~/.bashrc
```
Save content in it:
```
alias log='sudo journalctl -u remotepowserver -f --no-hostname -o cat'
alias start='sudo systemctl start remotepowserver'
alias restart='sudo systemctl restart remotepowserver'
alias stop='sudo systemctl stop remotepowserver'
alias build='go build -o /home/USERNAMEHERE/remotePoW/build/remotePowServer ./server'
```
Update the file 
```
source ~/.bashrc
```

Now you can run directly on the console:

*log* - Shows service logs

*start* - Starts service 

*restart* - Restarts service 

*stop* - Stops service

*build* - Builds server source code

