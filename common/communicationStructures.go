package common

type GetCommandRequest struct {
	Command string `json:"command"`
	Trytes  string `json:"trytes"`
	MWM     int    `json:"mwm"`
}

type DoPowResponse struct {
	TrytesWithPow string `json:"trytesWithPow"`
	MWM           int    `json:"mwm"`
}

type GetServerVersionResponse struct {
	ServerVersion string `json:"serverVersion"`
}

type GetPoWTypeResponse struct {
	PoWType string `json:"powType"`
}

type GetPoWVersionResponse struct {
	PoWVersion string `json:"powVersion"`
}

type GetPoWInfoResponse struct {
	ServerVersion string `json:"serverVersion"`
	PoWType       string `json:"powType"`
	PoWVersion    string `json:"powVersion"`
}
